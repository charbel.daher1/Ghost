# Use an official Node.js runtime as the base image
FROM node:18.12.1

# Set the working directory in the Docker image
WORKDIR /usr/src/app

# Copy the application code to the working directory
COPY . .

RUN yarn global add node-gyp

RUN yarn global add knex-migrator ember-cli

# Install the application dependencies
RUN yarn setup

# Define the command to run the application
CMD [ "yarn", "dev"]

# Expose the application on port 2368
EXPOSE 2368
